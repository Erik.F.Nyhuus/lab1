package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class RockPaperScissors {

    public static void main(String[] args) {
        
        /* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    // Objects
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> ynChoices = Arrays.asList("y", "n");

    
    public void run() {
        
        System.out.println("Let's play round " + roundCounter);
        
        String humanAnswer = readInput("Your choice (Rock/Paper/Scissors)?");
        // the game
        while (validHumanChoice(humanAnswer)){
            System.out.println(whoWins(humanAnswer, computerChoice()));
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continueOrNot = readInput("Do you wish to continue playing? (y/n)?");

            if (continueOrNot.equals("y")){
                run();
            }
            else if (continueOrNot.equals("n")){ 
                System.out.println("Bye bye :)");
                break;
            }
            else{
                while (!vaildYesNoChoice(continueOrNot)) {
                    continueOrNot = readInput("Do you wish to continue playing? (y/n)?");
                    humanAnswer = readInput("Your choice (Rock/Paper/Scissors)?"); 
                }
            }
        }
        while (!validHumanChoice(humanAnswer)){ 
            System.out.println("I do not understand " + humanAnswer + ". Could you try again?");
            humanAnswer = readInput("Your choice (Rock/Paper/Scissors)?");
            break;
        }

    }
    // Returns true or false if human input is vaild 
    public boolean validHumanChoice(String humanChoice){
        return rpsChoices.contains(humanChoice);
    }

    // Returns boolean if human input is vaild checks yes or no question. 
    public boolean vaildYesNoChoice(String humanChoice){
        return ynChoices.contains(humanChoice);
    } // not used

    // Returns random String choice from rpsChoices.
    public String computerChoice(){
        Random rand = new Random();
        int randComputerCoices = rand.nextInt(3);
        return rpsChoices.get(randComputerCoices);
    }

    /** compares the two
     * @param humanAnswer
     * @param computerAnswer
     * and returns the info string with both answer and who won.
    */
    /** if computerAnswer = rock and humanAnswer = scissors, 
     * the if statment is true
     * since the value: is "rock", rock beats scissors and computer wins.
     * Returns false if human wins.
     */

    public String whoWins(String humanAnswer, String computerAnswer){
        HashMap<String, String> opponentHashMap = new HashMap<String, String>();
        opponentHashMap.put("rock", "paper");
        opponentHashMap.put("paper", "scissors");
        opponentHashMap.put("scissors", "rock");

        String resultString = "";

        if (humanAnswer.equals(computerAnswer)){ // Tie both answer the same
            resultString = "It's a tie!";
            roundCounter += 1;
        }
        else if (computerAnswer.equals(opponentHashMap.get(humanAnswer))){ // Computer wins
            resultString = "Computer wins!";
            roundCounter += 1;
            computerScore += 1;
        }
        else{                               // Human wins
            resultString = "Human wins!";
            roundCounter += 1;
            humanScore += 1;
        }
        return ("Human chose " + 
                humanAnswer +  ", computer chose " + 
                computerAnswer + ". " + 
                resultString);
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
