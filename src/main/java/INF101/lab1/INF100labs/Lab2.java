package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;



/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        ArrayList<String> arrayWords = new ArrayList<>(Arrays.asList(word1, word2, word3));

        ArrayList<String> longestWords = new ArrayList<>();

        int currentWordLength = 0;
        for (String checkingWord : arrayWords){
            if (checkingWord.length() > currentWordLength){
                currentWordLength = checkingWord.length();
                longestWords.clear();
                longestWords.add(checkingWord);
            }   else if (checkingWord.length() == currentWordLength){
                    longestWords.add(checkingWord);

            }

        }
        for (String i : longestWords){
            System.out.println(i);
        }

    }

    public static boolean isLeapYear(int year) {
        if ((year % 100) == 0 && (year % 400) != 0) {
            return false;
        } else if((year % 4) < 1) {
            return true;    
        } else
            return false;
    }
    /*return year % 4 ==0 */

    public static boolean isEvenPositiveInt(int num) {
        if (num < 0){
            return false;
        } else if ((num > 0) && (num % 2) > 0){
                return false;
        } else
            return true;
    }

        

    

}
