package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> dobbeled = new ArrayList<>();
        for (int i : list){
            dobbeled.add(i * 2);
        }
        return dobbeled;
        // Nice
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i ++){
            if (Objects.equals(3, list.get(i))){
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> noDups = new ArrayList<>();
        for (int checker : list){
            if (noDups.contains(checker) == false){ // inverts the returnvalue from contain() method. 
                noDups.add(checker);
            
            }
        }
        return noDups;
    }
    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < b.size(); i++){
            int sumOfLists = a.get(i) + b.get(i);
            a.set(i, sumOfLists);
        }
        System.out.println(a);
    }

}