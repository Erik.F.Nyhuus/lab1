package INF101.lab1.INF100labs;



/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        crossSum(123);

    }

    public static void multiplesOfSevenUpTo(int n) {
        int partsOfSeven = n / 7;
        for (int counter = 1; (counter <= partsOfSeven); counter++){
            System.out.println(7 * counter);
        }
    }

    public static void multiplicationTable(int n) {
        for (int col = 1; col <= n; col++){
            System.out.print(col + ": ");
            for (int row  = 1; row <= n; row++){
                System.out.print(row * col + " ");
            }
            System.out.println(" ");
        }
        }

    public static int crossSum(int num) {    
            int sum = 0;
            while (num != 0)
            {
                sum = sum + num % 10;
                num = num/10;
            }
         
        return sum;


    }

}