package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;





/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {


    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
        grid.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));
        allRowsAndColsAreEqualSum(grid);
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        for (int i = 0; i < grid.size(); i++){
            System.out.println(grid.get(i));
        }

    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        
        // If grid passes all the tests the 
        boolean indicator = true; 
        
        // Finds the sum of the first row
        ArrayList<Integer> firstRow = new ArrayList<>(); 
        firstRow = grid.get(0);
        int sumOfRow = 0;
        for (int itRow : firstRow){
            sumOfRow += itRow;
        }

        // Finds the sum of the first coloum
        int sumOfCol = 0;
        for (int itCol = 0; itCol < grid.size(); itCol++){
            sumOfCol += grid.get(itCol).get(0);
        }
        
        // All rows in one list
        int sumOfNextRow = 0;
        ArrayList<Integer> sumAllRows = new ArrayList<>(); 
        ArrayList<Integer> nextRow = new ArrayList<>(); 
    
        for (int i = 0; i < grid.size(); i++ ){
            nextRow = grid.get(i);
            for (int j : nextRow){
                sumOfNextRow += j;
            }
            sumAllRows.add(sumOfNextRow);
            sumOfNextRow = 0;
        }

        // All cols in one list
        int sumOfNextCol = 0;
        ArrayList<Integer> sumAllCols = new ArrayList<>(); 
        ArrayList<Integer> nextCol = new ArrayList<>(); 
        
        for (int uCol = 0; uCol < grid.size(); uCol++){
            nextCol = grid.get(uCol);

            for (int jCol : nextCol){
                sumOfNextCol += jCol;
            }
            sumAllCols.add(sumOfNextCol);
            sumOfNextCol = 0;

        }

        int iteratorRow = 0;
        int iteratorCol = 0;
        if (sumAllRows.get(iteratorRow) == sumOfRow){
            iteratorRow++;
        }
        else
            indicator = false;

        if (sumAllCols.get(iteratorCol) == sumOfCol){
            iteratorCol++;
        }
        else
            indicator = false;
        
        return indicator;
    }
        


    
}